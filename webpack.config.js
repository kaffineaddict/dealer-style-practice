const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');

module.exports = {
  entry: {
    'ams_single_ford.min': [
      path.resolve(__dirname, 'style/vendor/bootstrap.css'),
      path.resolve(__dirname, 'style/pages/ams_single_page.css'),
      path.resolve(__dirname, 'style/pages/ams_disclaimer_overlay.css'),
      path.resolve(__dirname, 'style/pages/ams_card_flip.css'),
      path.resolve(__dirname, 'style/vendor/main.css'),
      path.resolve(__dirname, 'style/makes/ams_ford.css')
    ],
    'ams_specials_ford.min': [
      path.resolve(__dirname, 'style/vendor/bootstrap.css'),
      path.resolve(__dirname, 'style/pages/ams_specials_page.css'),
      path.resolve(__dirname, 'style/pages/ams_disclaimer_overlay.css'),
      path.resolve(__dirname, 'style/pages/ams_card_flip.css'),
      path.resolve(__dirname, 'style/vendor/main.css'),
      path.resolve(__dirname, 'style/makes/ams_ford.css'),
    ],
    'ams_single_mitsubishi.min': [
      path.resolve(__dirname, 'style/vendor/bootstrap.css'),
      path.resolve(__dirname, 'style/pages/ams_single_page.css'),
      path.resolve(__dirname, 'style/pages/ams_disclaimer_overlay.css'),
      path.resolve(__dirname, 'style/pages/ams_card_flip.css'),
      path.resolve(__dirname, 'style/vendor/main.css'),
      path.resolve(__dirname, 'style/makes/ams_mitsubishi.css')
    ],
    'ams_specials_mitsubishi.min': [
      path.resolve(__dirname, 'style/vendor/bootstrap.css'),
      path.resolve(__dirname, 'style/pages/ams_specials_page.css'),
      path.resolve(__dirname, 'style/pages/ams_disclaimer_overlay.css'),
      path.resolve(__dirname, 'style/pages/ams_card_flip.css'),
      path.resolve(__dirname, 'style/vendor/main.css'),
      path.resolve(__dirname, 'style/makes/ams_mitsubishi.css'),
    ],
    'ams_single_honda.min': [
      path.resolve(__dirname, 'style/vendor/bootstrap.css'),
      path.resolve(__dirname, 'style/pages/ams_single_page.css'),
      path.resolve(__dirname, 'style/pages/ams_disclaimer_overlay.css'),
      path.resolve(__dirname, 'style/pages/ams_card_flip.css'),
      path.resolve(__dirname, 'style/vendor/main.css'),
      path.resolve(__dirname, 'style/makes/ams_honda.css')
    ],
    'ams_specials_honda.min': [
      path.resolve(__dirname, 'style/vendor/bootstrap.css'),
      path.resolve(__dirname, 'style/pages/ams_specials_page.css'),
      path.resolve(__dirname, 'style/pages/ams_disclaimer_overlay.css'),
      path.resolve(__dirname, 'style/pages/ams_card_flip.css'),
      path.resolve(__dirname, 'style/vendor/main.css'),
      path.resolve(__dirname, 'style/makes/ams_honda.css'),
    ],
  },
  output: {
    filename: '[name]',
    path: path.resolve(__dirname, 'dist'),
  },
  module: {
    rules: [
      {
        test: /\.css$/i,
        use: [MiniCssExtractPlugin.loader, 'css-loader'],
      },
    ],
  },
  optimization: {
    minimizer: [
      new CssMinimizerPlugin(),
    ],
  },
  plugins: [new MiniCssExtractPlugin()],
};
